ASSIGNMENT 1

''Softmax-Classifier for CIFAR-10'''

from __future__ import absolute_import 
from __future__ import division
from __future__ import print_function  \\ these commands are used to ensure th compatability of python 2and 3.

import numpy as np \\ numerical calculations
import tensorflow as tf \\ numerical calculations
import time \\ time module 
import data_helpers \\ loading the data set 

beginTime = time.time() \\ for measuring the run time 

# Parameter definitions
batch_size = 100
learning_rate = 0.005
max_steps = 1000

# Uncommenting this line removes randomness
# You'll get exactly the same result on each run
# np.random.seed(1)

# Prepare data
data_sets = data_helpers.load_data() \\ data set and loading of data set

# -----------------------------------------------------------------------------
# Prepare the TensorFlow graph
# (We're only defining the graph here, no actual calculations taking place)
# -----------------------------------------------------------------------------

# Define input placeholders
images_placeholder = tf.placeholder(tf.float32, shape=[None, 3072]) \\ place holders will hold the size and shape\\ specifying input datas type and shape . tf.float32 represents the floating point values.
labels_placeholder = tf.placeholder(tf.int64, shape=[None]) \\tf.int64 represents the integer values from 0 to 9. \\ 3072 is the                                                               floating point values of a single image.

# Define variables (these are the values we want to optimize)
weights = tf.Variable(tf.zeros([3072, 10]))\\ weights are added with each of the values.
biases = tf.Variable(tf.zeros([10])) 

# Define the classifier's result
logits = tf.matmul(images_placeholder, weights) + biases \\the dimensions of the vectors and matrices are arranged in such a way                                                            that  multiple images calculated in a single step. 

# Define the loss function
loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,
  labels=labels_placeholder)) \\  the input training data is allowed to make predictions using current parameters. this                                           parameter is then compared to class labels. loss is a function of the numerical result.

# Define the training operation
train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss) \\gradient descent is a term which is calculated                                                                                  for loss functions. by using optimization                                                                                        techniques for translating the information into                                                                                  actual parameter updates. 

# Operation comparing prediction with true label
correct_prediction = tf.equal(tf.argmax(logits, 1), labels_placeholder)  \\ argmax of logits along dimension 1 returns the                                                                                   indices of the class with the highest score.The                                                                                 labels are then compared to the correct class labels                                                                             by tf.equal(), which returns a vector of boolean values.

# Operation calculating the accuracy of our predictions
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32)) //These two lines measure the model’s accuracy.

# -----------------------------------------------------------------------------
# Run the TensorFlow graph
# -----------------------------------------------------------------------------

with tf.Session() as sess:
  # Initialize variables
  sess.run(tf.global_variables_initializer()) \\The first thing we do after launching the session is initializing the variables                                                 we created earlier

  # Repeat max_steps times
  for i in range(max_steps):

    # Generate input data batch
    indices = np.random.choice(data_sets['images_train'].shape[0], batch_size)
    images_batch = data_sets['images_train'][indices]
    labels_batch = data_sets['labels_train'][indices] \\ these lines randomly pick a certain number of images from the training                                                          data.The resulting chunks of images and labels from the training data                                                            are called batches. 

    # Periodically print out the model's current accuracy
    if i % 100 == 0:
      train_accuracy = sess.run(accuracy, feed_dict={
        images_placeholder: images_batch, labels_placeholder: labels_batch})
      print('Step {:5d}: training accuracy {:g}'.format(i, train_accuracy)) \\ Every 100 iterations we check the model’s current                                                                                accuracy on the training data batch. 

    # Perform a single training step
    sess.run(train_step, feed_dict={images_placeholder: images_batch,
      labels_placeholder: labels_batch}) \\ here we are telling the model to perform one single trainig step.

  # After finishing the training, evaluate on the test set
  test_accuracy = sess.run(accuracy, feed_dict={
    images_placeholder: data_sets['images_test'],
    labels_placeholder: data_sets['labels_test']})
  print('Test accuracy {:g}'.format(test_accuracy)) \\ After the training is completed, we evaluate the model on the test set.                                                          This is the first time the model ever sees the test set, so the images in                                                        the test set are completely new to the model.

endTime = time.time()
print('Total time: {:5.2f}s'.format(endTime - beginTime))
